﻿using UnityEngine;
using System.Collections;
using Leap;

// Controls the cursor through Leap Motion
public class MainMenu : LeapController
{
    public GUIText LeapText;
    public GUITexture Game;
    public GUITexture Tutorial;
    public GUITexture Quit;

    private bool tutorialHit;
    private bool gameHit;
    private bool quitHit;

    protected override void AtStart()
    {
        float baseHeightInverted = 1 / BaseHeight;
        originalButtonHeight = Game.pixelInset.height;
        float ratio = UnityEngine.Screen.height * baseHeightInverted;  
        Game.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        Tutorial.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        Quit.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        originalButtonHeight = Game.pixelInset.height;
    }

    protected override void CursorBehavior()
    {
        if (controller.IsConnected)
        {
            LeapText.text = "Leap connected";
        }
        else
        {
            LeapText.text = "Leap not connected";
        }

        // cursor's screen position
        Vector3 position = new Vector3(
             GetComponent<GUITexture>().transform.position.x * UnityEngine.Screen.width, 
             GetComponent<GUITexture>().transform.position.y * UnityEngine.Screen.height, 
             GetComponent<GUITexture>().transform.position.z);

        tutorialHit = Tutorial.HitTest(position);
        gameHit = Game.HitTest(position);
        quitHit = Quit.HitTest(position);

        if (tutorialHit || gameHit || quitHit)
        {
            if (!selected)
            {
                stopTime = Time.time + waitTime;
                selected = true;
                GetComponent<GUITexture>().texture = Green;
                TimerText.transform.position = GetComponent<GUITexture>().transform.position;
                TimerText.transform.Translate(new Vector3(0.0f, -0.05f, 1.0f));
                TimerText.text = waitTime.ToString();
                TimerText.enabled = true;

                if (tutorialHit)
                {
                    Tutorial.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
                else if (gameHit)
                {
                    Game.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
                else
                {
                    Quit.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
            }
            else 
            {
                float time = Time.time;
                if (time >= stopTime)
                {
                    if (quitHit)
                    {
                        Application.Quit();
                    }
                    else if (gameHit)
                    {
                        Application.LoadLevel("game");
                    }
                    else if (tutorialHit)
                    {
                        Application.LoadLevel("tutorial");
                    }
                }
                else
                {
                    TimerText.text = (stopTime - time).ToString("F2");
                }
            }
        }
        else
        {
            Tutorial.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);
            Game.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);
            Quit.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);

            GetComponent<GUITexture>().texture = Yellow;
            selected = false;
            TimerText.enabled = false;
        }
    }
}