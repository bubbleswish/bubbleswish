﻿using UnityEngine;
using System.Collections;

// pops bubbles that are out of view
public class GravityPopper : MonoBehaviour 
{
    public GameController GameController;

    private void OnTriggerExit(Collider other) 
    {
        if (other.tag == "Burstable") 
        {
            Destroy(other.gameObject);
            GameController.DecreaseSoaps();
        }
    }
}