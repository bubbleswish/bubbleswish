﻿using UnityEngine;
using System.Collections;

// pops bubble when it crashes into another bubble
public class Pop : MonoBehaviour 
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Burstable") 
        {
            Destroy(gameObject);
        }
    }
}