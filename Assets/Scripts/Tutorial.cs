﻿using UnityEngine;
using System.Collections;
using Leap;

// Controls the cursor through Leap Motion
public class Tutorial : LeapController
{
    public GUITexture Instructions;
    public GUITexture GoodJobMessage;
    public GUITexture MainButton;
    public GUITexture GameButton;
	public AudioClip PopSound;

    private bool mainMenuHit;
    private bool newGameHit;
    
    protected override void AtStart()
    {
        mainMenuHit = false;
        newGameHit = false;
        GameButton.enabled = false;
        float baseHeightInverted = 1 / BaseHeight;

        originalButtonHeight = MainButton.pixelInset.height;

        float ratio = UnityEngine.Screen.height * baseHeightInverted;  
        MainButton.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        GameButton.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        originalButtonHeight = MainButton.pixelInset.height;
    }

    protected override void CursorBehavior()
    {
        // cursor's screen position
        Vector3 position = new Vector3(
            GetComponent<GUITexture>().transform.position.x * UnityEngine.Screen.width, 
            GetComponent<GUITexture>().transform.position.y * UnityEngine.Screen.height, 
            0);

        // check if the cursor is over a button
        mainMenuHit = MainButton.HitTest(position);
        newGameHit = GameButton.HitTest(position);
        if (mainMenuHit || (newGameHit && GameButton.enabled))
        {
            if (!selected) 
            {
                stopTime = Time.time + waitTime;
                selected = true;
                GetComponent<GUITexture>().texture = Green;
                TimerText.transform.position = GetComponent<GUITexture>().transform.position;
                TimerText.transform.Translate(new Vector3(0.0f, -0.05f, 0.0f));
                TimerText.text = waitTime.ToString();
                TimerText.enabled = true;

                if (mainMenuHit)
                {
                    MainButton.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
                else
                {
                    GameButton.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
            } 
            else 
            {
                float time = Time.time;
                if (time >= stopTime)
                {
                    if (mainMenuHit)
                    {
                        Application.LoadLevel("mainMenu");
                    }
                    else
                    {
                        Application.LoadLevel("game");
                    }
                }
                else
                {
                    TimerText.text = (stopTime - time).ToString("F2");
                }
            }
        }
        else
        {
            selected = false;
            TimerText.enabled = false;
            MainButton.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);
            GameButton.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);

            RaycastHit hit;
            Ray testRay = Camera.main.ViewportPointToRay(GetComponent<GUITexture>().transform.position);
            if (Physics.Raycast(testRay, out hit))
            {
                Vector velocity = frame.Pointables.Frontmost.TipVelocity;
                if (hit.collider.gameObject.tag == "Burstable")
                {
                    GetComponent<GUITexture>().texture = Green;
                    if (velocity.z < fingerZvelocity)
                    {
                        // destroy the bubble
                        Destroy(hit.collider.gameObject);
						GetComponent<AudioSource>().PlayOneShot(PopSound);
                        Instructions.enabled = false;
                        GoodJobMessage.enabled = true;
                        GameButton.enabled = true;
                    }
                }
                else
                {
                    GetComponent<GUITexture>().texture = Yellow;
                }
            }
            else
            {
                GetComponent<GUITexture>().texture = Yellow;
            }
        }
    }
}