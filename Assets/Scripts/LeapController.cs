﻿using UnityEngine;
using System.Collections;
using Leap;

// Controls the cursor through Leap Motion
public class LeapController : MonoBehaviour
{
    public GUIText TimerText;
    public Texture Green;
    public Texture Yellow;

    protected const float BaseWidth = 1024.0f;
    protected const float BaseHeight = 768.0f;
    protected static float waitTime = 1.0f;
    protected static float fingerZvelocity = -200.0f; // mm/s
    protected float originalButtonHeight; // originalPixelInsetHeight
    protected Controller controller;
    protected Frame lastFrame;
    protected Frame frame;
    protected bool selected;
    protected float stopTime;

    // depends on each scene
    protected virtual void CursorBehavior()
    {
    }

    // depends on each scene
    protected virtual void AtStart()
    {
    }

    private void Awake()
    {
        controller = new Controller();
    }

    private void Start()
    {
        if (controller.IsConnected)
        {
            lastFrame = controller.Frame();
        }

        lastFrame = new Frame();
        frame = new Frame();
        Cursor.visible = false;
        selected = false;
        stopTime = 0;
        AtStart();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Cursor.visible = true;
            Application.Quit();
        }

        if (controller.IsConnected) 
        { 
            frame = controller.Frame();
            if (frame.IsValid)
            {
                if (frame.Id != lastFrame.Id)
                {
                    InteractionBox interBox = frame.InteractionBox;
                    Pointable finger = frame.Pointables.Frontmost;
                    Vector normalizedPos = interBox.NormalizePoint(finger.StabilizedTipPosition);
                    
                    if (frame.Pointables.Count > 0 && frame.TranslationProbability(lastFrame) > 0.60)
                    {
                        Vector3 pointablePos = new Vector3(
                                                normalizedPos.x, 
                                                normalizedPos.y, 
                                                1.0f);
                        GetComponent<GUITexture>().transform.position = pointablePos;
                        CursorBehavior();
                    }

                    lastFrame = frame;
                }
            }
        }
    }

    private void OnDestroy() 
    {
        controller.Dispose();
    }
}