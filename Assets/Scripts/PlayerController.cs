﻿using UnityEngine;
using System.Collections;
using Leap;
 
// Controls the cursor through Leap Motion
public class PlayerController : LeapController
{
    public GameController GameController;
    public GUITexture MainButton;
    public GUITexture PlayButton;
    public AudioClip PopSound;
    public GUITexture Points;

    private static float pointsWait = 1.0f;
    private float pointsTimer;
    private bool mainMenuHit;
    private bool newGameHit;

    protected override void AtStart()
    {
        mainMenuHit = false;
        newGameHit = false;

        float baseHeightInverted = 1 / BaseHeight;
        
        originalButtonHeight = MainButton.pixelInset.height;
        
        float ratio = UnityEngine.Screen.height * baseHeightInverted;  
        MainButton.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        PlayButton.pixelInset = new Rect(0, 0, originalButtonHeight * ratio, originalButtonHeight * ratio);
        originalButtonHeight = MainButton.pixelInset.height;
    }

    protected override void CursorBehavior()
    {
        if (Points.enabled && (Time.time >= pointsTimer))
        {
            Points.enabled = false;
        }

        // cursor's screen position
        Vector3 position = new Vector3(
            GetComponent<GUITexture>().transform.position.x * UnityEngine.Screen.width,
            GetComponent<GUITexture>().transform.position.y * UnityEngine.Screen.height,
            GetComponent<GUITexture>().transform.position.z);

        // check if the cursor is over a button
        mainMenuHit = MainButton.HitTest(position);
        newGameHit = PlayButton.HitTest(position);

        if (mainMenuHit || (newGameHit && PlayButton.enabled))
        {
            if (!selected) 
            {
                stopTime = Time.time + waitTime;
                selected = true;
                GetComponent<GUITexture>().texture = Green;
                TimerText.transform.position = GetComponent<GUITexture>().transform.position;
                TimerText.transform.Translate(new Vector3(0.0f, -0.05f, 1.0f));
                TimerText.text = waitTime.ToString();
                TimerText.enabled = true;

                if (mainMenuHit)
                {
                    MainButton.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
                else
                {
                    PlayButton.pixelInset = new Rect(0, 0, originalButtonHeight * 1.5f, originalButtonHeight * 1.5f);
                }
            } 
            else 
            {
                float time = Time.time;
                if (time >= stopTime) 
                {
                    if (mainMenuHit)
                    {
                        Application.LoadLevel("mainMenu");
                    }
                    else
                    {
                        Application.LoadLevel(Application.loadedLevel);
                    }
                }
                else
                {
                    TimerText.text = (stopTime - time).ToString("F2");
                }
            }
        }
        else
        {
            selected = false;
            TimerText.enabled = false;
            MainButton.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);
            PlayButton.pixelInset = new Rect(0, 0, originalButtonHeight, originalButtonHeight);
            
            RaycastHit hit;
            Ray testRay = Camera.main.ViewportPointToRay(GetComponent<GUITexture>().transform.position);
            if (Physics.Raycast(testRay, out hit)) 
            {
                Vector velocity = frame.Pointables.Frontmost.TipVelocity;
                if (hit.collider.gameObject.tag == "Burstable") 
                {
                    GetComponent<GUITexture>().texture = Green;
                    if (velocity.z < fingerZvelocity)
                    {
                        Points.transform.position = GetComponent<GUITexture>().transform.position;
                        Points.transform.Translate(new Vector3(0.0f, 0.05f, 0.0f));

                        // destroy the bubble
                        Destroy(hit.collider.gameObject);
                        GameController.IncreaseScore();
                        GetComponent<AudioSource>().PlayOneShot(PopSound);
                        Points.enabled = true;
                        pointsTimer = Time.time + pointsWait;
                    }
                } 
                else 
                {
                    GetComponent<GUITexture>().texture = Yellow;
                }
            } 
            else
            {
                GetComponent<GUITexture>().texture = Yellow;
            }
        }
    }
}