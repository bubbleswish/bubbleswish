﻿using UnityEngine;
using System.Collections;

public class Floter : MonoBehaviour 
{
    public float Speed;

    private void Start()
    {
        Speed = 1.0f;
        GetComponent<Rigidbody>().velocity = transform.up * Speed;
    }
}