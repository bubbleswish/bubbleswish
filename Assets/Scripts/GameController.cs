﻿using UnityEngine;
using System.Collections;

// main game
public class GameController : MonoBehaviour 
{
    public GameObject Bubble;
    public GameObject BubbleClones;
    public GUIText ScoreText;
    public GUITexture LoseText;
    public GUITexture PlayButton;
    public GUITexture SoapLiveTex1;
    public GUITexture SoapLiveTex2;
    public GUITexture SoapLiveTex3;
    public GUITexture SoapLiveTex4;
    public GUITexture SoapLiveTex5;
    public Transform Mirror;
    
    private static float startWait = 1.0f;
    private static int maxBubbles = 7;
    private Vector3 boundsVector;
    private float waveWait;
    private float spawnWait;
    private int bubbleCount;
    private int score;
    private int prevScore;
    private int soaps;
    private bool gameOn;
    private float bubbleSpeed;
    private WebCamTexture webcamTexture;

    public void IncreaseScore()
    {
        if (gameOn)
        {
            score = score + 5;
            ScoreText.text = "Score: " + score;
            if ((score >= 25 + prevScore) && (spawnWait >= 0.5f))
            {
                if (bubbleCount < maxBubbles)
                {
                    bubbleCount++;
                }

                spawnWait = spawnWait - 0.25f;
                waveWait = waveWait - 0.25f;
                bubbleSpeed = bubbleSpeed + 0.2f;
                prevScore = score;
            }
        }
    }

    public void DecreaseSoaps()
    {
        if (gameOn)
        {
            soaps--;
            SoapLiveTex1.enabled = false;
            if (soaps == 0)
            {
                PlayButton.enabled = true;
                LoseText.enabled = true;
                gameOn = false;
                SoapLiveTex5.enabled = false;
                foreach (Transform child in BubbleClones.transform)
                {
                    Destroy(child.gameObject);
                }
            }
            else if (soaps == 1)
            {
                SoapLiveTex4.enabled = false;
            }
            else if (soaps == 2)
            {
                SoapLiveTex3.enabled = false;
            }
            else if (soaps == 3)
            {
                SoapLiveTex2.enabled = false;
            }
        }
    }

    private void Start()
    {
        gameOn = true;
        bubbleCount = 1;
        waveWait = 4.0f;
        spawnWait = 1.0f;
        score = 0;
        prevScore = 0;
        soaps = 5;
        boundsVector = new Vector3(5.0f, 4.5f, 2.0f);
        bubbleSpeed = 1.5f;
        StartCoroutine(SpawnWaves());
        SoapLiveTex1.enabled = true;
        SoapLiveTex2.enabled = true;
        SoapLiveTex3.enabled = true;
        SoapLiveTex4.enabled = true;
        SoapLiveTex5.enabled = true;
        PlayButton.enabled = false;

        // Show webcam for demos
        /*WebCamDevice[] devices = WebCamTexture.devices;
        if (devices.Length > 0)
        {
            mirror.GetComponent<Renderer>().enabled = true;
            webcamTexture = new WebCamTexture ();
            webcamTexture.deviceName = devices[0].name;
            Debug.Log(webcamTexture.deviceName);
            mirror.GetComponent<Renderer>().material.mainTexture = webcamTexture;
            webcamTexture.Play();
        }*/
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.LoadLevel("mainMenu");
        }
    }

    private IEnumerator SpawnWaves()
    {
        Vector3 boundVec = boundsVector;
        yield return new WaitForSeconds(startWait);
        while (gameOn)
        {
            for (int i = 0; i < bubbleCount; i++)
            {
                Vector3 spawnPosition = new Vector3(Random.Range(-boundVec.x, boundVec.x), -boundVec.y, Random.Range(-boundVec.z, boundVec.z));
                Quaternion spawnRotation = Quaternion.identity;
                var clone = (GameObject)Instantiate(Bubble, spawnPosition, spawnRotation);
                clone.GetComponent<Rigidbody>().velocity = transform.up * bubbleSpeed;
                clone.transform.parent = BubbleClones.transform;
                yield return new WaitForSeconds(spawnWait);
            }

            yield return new WaitForSeconds(waveWait);
        }
    }
}